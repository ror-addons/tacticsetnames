﻿<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="TacticSetNames" version="1.0" date="09/10/2010" >

		<VersionSettings gameVersion="1.3.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="Olmi" email="" />
		<Description text="Adds descriptions of your tacticsets to the tacticset selection. To open the settingswindow just right click the tacticset selection button." />

		<Dependencies>
			<Dependency name="EASystem_WindowUtils" />
			<Dependency name="EASystem_Utils" />
			<Dependency name="EA_TacticsWindow" />
		</Dependencies>

		<SavedVariables>
			<SavedVariable name="TacticSetNames.SavedSettings" />
		</SavedVariables>

		<Files>
			<File name="textures/textures.xml" />
			<File name="source/TacticSetNames.xml" />
			<File name="source/TacticSetNames.lua" />
			<File name="source/Settings.lua" />
		</Files>

		<OnInitialize>
				<CallFunction name="TacticSetNames.Initialize" />
		</OnInitialize>

		<OnShutdown>
            	<CallFunction name="TacticSetNames.Shutdown" />
		</OnShutdown>

		<WARInfo>
			<Categories>
				<Category name="COMBAT" />
				<Category name="OTHER" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>

	</UiMod>
</ModuleFile>
