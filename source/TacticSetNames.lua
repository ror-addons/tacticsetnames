TacticSetNames =
{
	MAX_TACTIC_SETS = GameData.MAX_TACTICS_SETS,
	templateName = "TacticSetNames_Labels",
	windowName = "TacticSetNames",
	eabuttonname = "EA_TacticsEditorContentsSetMenuMenuButton",
	Settings = nil,
}

local old
local Settings

local function hook()
	for i = 1, TacticSetNames.MAX_TACTIC_SETS do
		if ( DoesWindowExist(TacticSetNames.eabuttonname..i) and not DoesWindowExist(TacticSetNames.windowName..i) ) then
			CreateWindowFromTemplate( TacticSetNames.windowName..i, TacticSetNames.templateName, TacticSetNames.eabuttonname..i )

			WindowSetScale(TacticSetNames.windowName..i, WindowGetScale("EA_TacticsEditorContentsSetMenu"))
			WindowClearAnchors(TacticSetNames.windowName..i)
			WindowAddAnchor(TacticSetNames.windowName..i, "right", TacticSetNames.eabuttonname..i, "left", 5, 0)

			TacticSetNames.UpdateLabel(i)
		end
	end

	TacticSetNames.SettingsWindow.CreateWindow()
end

function TacticSetNames.Initialize()
	TacticSetNames.CheckSettings()

	if ( TacticsEditor ) then
		old = TacticsEditor.HandleLoadingEnd
		TacticsEditor.HandleLoadingEnd =
			function(...)
				old(...)
				hook()
			end

		RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED,	"TacticSetNames.OnReload")
	else
		-- something is wrong here ...
	end
end

function TacticSetNames.OnReload()
	hook()
end

function TacticSetNames.Shutdown()
	if ( old ) then
		TacticsEditor.HandleLoadingEnd = old
	end
end

function TacticSetNames.UpdateAllLabels()
	for i = 1, TacticSetNames.MAX_TACTIC_SETS do
		TacticSetNames.UpdateLabel(i)
	end
end

function TacticSetNames.UpdateLabel(index)
	if ( index <= 0 or index > TacticSetNames.MAX_TACTIC_SETS ) then
		return
	end

	LabelSetText( TacticSetNames.windowName..index, Settings.Tactics[index].name )
	LabelSetFont( TacticSetNames.windowName..index, Settings.Font, 10 )

	local color = Settings.Tactics[index].color
	LabelSetTextColor( TacticSetNames.windowName..index, color.r, color.g, color.b )

	ButtonSetTextColor( TacticSetNames.eabuttonname..index, Button.ButtonState.NORMAL, color.r, color.g, color.b )
end

function TacticSetNames.CheckSettings()
	if ( not TacticSetNames.SavedSettings ) then
		TacticSetNames.SavedSettings = {}
	end

	local index = L"" .. towstring(GameData.Account.ServerName) .. L"-" .. towstring(GameData.Player.name)

	if ( not TacticSetNames.SavedSettings[index] ) then
		TacticSetNames.SavedSettings[index] = { version = 1.0, Tactics = {}, Font = "font_clear_medium_bold" }

		for i=1, TacticSetNames.MAX_TACTIC_SETS do
			TacticSetNames.SavedSettings[index].Tactics[i] =
			{
				id = i,
				name = towstring("Set " .. i),
				color =
				{
					r = 255,
					g = 255,
					b = 255,
					a = 1,
				},
			}
		end
	end

	-- Check if Mythic implemented additional tactic sets of that we don't have SavedSettings
	if ( #TacticSetNames.SavedSettings[index].Tactics < TacticSetNames.MAX_TACTIC_SETS ) then
		for i=1, TacticSetNames.MAX_TACTIC_SETS do
			if ( not TacticSetNames.SavedSettings[index].Tactics[i] ) then
				TacticSetNames.SavedSettings[index].Tactics[i] =
				{
					id = i,
					name = towstring("Set " .. i),
					color =
					{
						r = 255,
						g = 255,
						b = 255,
						a = 1,
					},
				}
			end
		end
	end

	TacticSetNames.Settings = TacticSetNames.SavedSettings[index]
	Settings = TacticSetNames.Settings
end

