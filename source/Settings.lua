TacticSetNames.SettingsWindow = {}

local windowname = "TacticSetNamesSettingsWindow"

local Settings

local currentrowtoedit

local function UpdateColors(index)
	local color = Settings.Tactics[index].color
	WindowSetTintColor( windowname .. "Row" .. index .. "Color", color.r, color.g, color.b )
	ButtonSetTextColor( windowname .. "Row" .. index .. "TacticButton", Button.ButtonState.NORMAL, color.r, color.g, color.b )
end

local function UpdateEditBox(index)
	TextEditBoxSetText( windowname .. "Row" .. index .. "EditBox", Settings.Tactics[index].name )
end

local function SaveEditBox(index)
	Settings.Tactics[index].name = TextEditBoxGetText( windowname .. "Row" .. index .. "EditBox" )
	TacticSetNames.UpdateLabel( index )
end

local function SaveAll()
	for i = 1, TacticSetNames.MAX_TACTIC_SETS do
		SaveEditBox(i)
	end
end

local function UpdateAll()
	for i = 1, TacticSetNames.MAX_TACTIC_SETS do
		UpdateColors(i)
		UpdateEditBox(i)
	end
end

local function SetLabels()
	LabelSetText("TacticSetNamesSettingsWindowSelectFont", L"Selected Font")
	LabelSetFont("TacticSetNamesSettingsWindowSelectFont",Settings.Font,10)

	LabelSetText("TacticSetNamesSettingsWindowTitleLabel", L"TacticSetNames - Settings")

	LabelSetText("TacticSetNamesColorPickerRedSliderLabel", L"Red")
	LabelSetText("TacticSetNamesColorPickerGreenSliderLabel", L"Green")
	LabelSetText("TacticSetNamesColorPickerBlueSliderLabel", L"Blue")

	ButtonSetText("TacticSetNamesSettingsWindowSelectButton", L"Select Font")
end

local function UpdateSliders()
	if ( not currentrowtoedit ) then
		return
	end

	SliderBarSetCurrentPosition("TacticSetNamesColorPickerRedSlider", currentrowtoedit.color.r / 255)
	LabelSetText("TacticSetNamesColorPickerRedValue", towstring(currentrowtoedit.color.r))
	SliderBarSetCurrentPosition("TacticSetNamesColorPickerGreenSlider", currentrowtoedit.color.g / 255)
	LabelSetText("TacticSetNamesColorPickerGreenValue", towstring(currentrowtoedit.color.g))
	SliderBarSetCurrentPosition("TacticSetNamesColorPickerBlueSlider", currentrowtoedit.color.b / 255)
	LabelSetText("TacticSetNamesColorPickerBlueValue", towstring(currentrowtoedit.color.b))

	WindowSetTintColor("TacticSetNamesColorPickerColorPreview", currentrowtoedit.color.r, currentrowtoedit.color.g, currentrowtoedit.color.b)
end

local function CreateContextMenu()
	local size = #ChatSettings.Fonts
	for idx=1, size do
		if ( not DoesWindowExist("TSN_ContextFontMenuItem"..idx) ) then
			CreateWindowFromTemplate ("TSN_ContextFontMenuItem"..idx, "ChatContextMenuItemFontSelection", "Root")
		end
		LabelSetFont( "TSN_ContextFontMenuItem"..idx.."Label", ChatSettings.Fonts[idx].fontName, WindowUtils.FONT_DEFAULT_TEXT_LINESPACING )
		LabelSetText( "TSN_ContextFontMenuItem"..idx.."Label", StringToWString(ChatSettings.Fonts[idx].shownName) )
		local _, y = LabelGetTextDimensions( "TSN_ContextFontMenuItem"..idx.."Label" )
		local x, _ = WindowGetDimensions( "TSN_ContextFontMenuItem"..idx )
		WindowSetDimensions( "TSN_ContextFontMenuItem"..idx, x, y )
		WindowRegisterCoreEventHandler("TSN_ContextFontMenuItem"..idx, "OnLButtonUp", "TacticSetNames.SettingsWindow.ChangeFont")
		WindowSetShowing("TSN_ContextFontMenuItem"..idx, false)
		WindowSetId("TSN_ContextFontMenuItem"..idx, idx)
	end
end

function TacticSetNames.SettingsWindow.CreateWindow()
	CreateWindow( "TacticSetNamesSettingsWindow", false )
	CreateWindow( "TacticSetNamesColorPicker", false )

	WindowSetId( "TacticSetNamesColorPickerRedSlider", 1 )
	WindowSetId( "TacticSetNamesColorPickerGreenSlider", 2 )
	WindowSetId( "TacticSetNamesColorPickerBlueSlider", 3 )

	WindowRegisterCoreEventHandler("EA_TacticsEditorContentsSetMenu", "OnRButtonUp", "TacticSetNames.SettingsWindow.OpenSettingsWindow")

	Settings = TacticSetNames.Settings

	SetLabels()
	CreateContextMenu()

	for i = 1, TacticSetNames.MAX_TACTIC_SETS do
		ButtonSetText( windowname .. "Row" .. i .. "TacticButton", towstring(i) )
		WindowSetId( windowname .. "Row" .. i .. "Color", i )
		UpdateColors(i)
		UpdateEditBox(i)
	end
end

function TacticSetNames.SettingsWindow.OpenColorDialog()
	local id = WindowGetId( SystemData.ActiveWindow.name )

	currentrowtoedit = Settings.Tactics[id]

	if ( not currentrowtoedit ) then
		return
	end

	LabelSetText("TacticSetNamesColorPickerTitle", towstring("Tacticset "..id))

	UpdateSliders()

	WindowSetShowing( "TacticSetNamesColorPicker", true )
	WindowClearAnchors( "TacticSetNamesColorPicker" )
	WindowAddAnchor("TacticSetNamesColorPicker", "topright", "TacticSetNamesSettingsWindowBackground", "topleft", 0, 0)
end

function TacticSetNames.SettingsWindow.OpenSettingsWindow()
	WindowSetShowing( "TacticSetNamesSettingsWindow", true )
	WindowSetShowing( "TacticSetNamesColorPicker", false )

	UpdateAll()
end

function TacticSetNames.SettingsWindow.CloseSettingsWindow()
	currentrowtoedit = nil

	SaveAll()

	WindowSetShowing( "TacticSetNamesSettingsWindow", false )
	WindowSetShowing( "TacticSetNamesColorPicker", false )
end

local function UpdateColorPickerPreview()
	if ( not currentrowtoedit ) then
		return
	end

	WindowSetTintColor("TacticSetNamesColorPickerColorPreview", currentrowtoedit.color.r, currentrowtoedit.color.g, currentrowtoedit.color.b)
	TacticSetNames.UpdateLabel(currentrowtoedit.id)
	UpdateColors(currentrowtoedit.id)
end

function TacticSetNames.SettingsWindow.OnSlide(pos)
	local id = WindowGetId( SystemData.ActiveWindow.name )

	if ( id <= 0 or id > 3 ) then
		return
	end

	if ( id == 1 ) then
		local color = math.floor(255 * pos + 0.5)
		currentrowtoedit.color.r = color
		LabelSetText("TacticSetNamesColorPickerRedValue", towstring(color))
	elseif ( id == 2 ) then
		local color = math.floor(255 * pos + 0.5)
		currentrowtoedit.color.g = color
		LabelSetText("TacticSetNamesColorPickerGreenValue", towstring(color))
	elseif ( id == 3 ) then
		local color = math.floor(255 * pos + 0.5)
		currentrowtoedit.color.b = color
		LabelSetText("TacticSetNamesColorPickerBlueValue", towstring(color))
	else
		return
	end

	UpdateColorPickerPreview()
end

function TacticSetNames.SettingsWindow.OpenSelectFont()
	EA_Window_ContextMenu.CreateContextMenu("", EA_Window_ContextMenu.CONTEXT_MENU_1)
	local size = #ChatSettings.Fonts
	for idx=1, size
	do
		local _, y = LabelGetTextDimensions( "TSN_ContextFontMenuItem"..idx.."Label" )
		EA_Window_ContextMenu.AddUserDefinedMenuItem("TSN_ContextFontMenuItem"..idx, EA_Window_ContextMenu.CONTEXT_MENU_1)
		if ( ChatSettings.Fonts[idx].fontName == Settings.Font ) then
			WindowSetShowing("TSN_ContextFontMenuItem"..idx.."Check", true)
		else
			WindowSetShowing("TSN_ContextFontMenuItem"..idx.."Check", false)
		end
	end
	EA_Window_ContextMenu.Finalize(EA_Window_ContextMenu.CONTEXT_MENU_1)
end

function TacticSetNames.SettingsWindow.ChangeFont()
	local fontIndex = WindowGetId(SystemData.ActiveWindow.name)
	Settings.Font = ChatSettings.Fonts[fontIndex].fontName
	LabelSetFont("TacticSetNamesSettingsWindowSelectFont",Settings.Font,10)

	local size = #ChatSettings.Fonts
	for idx=1, size
	do
		if ( ChatSettings.Fonts[idx].fontName == Settings.Font ) then
			WindowSetShowing("TSN_ContextFontMenuItem"..idx.."Check", true)
		else
			WindowSetShowing("TSN_ContextFontMenuItem"..idx.."Check", false)
		end
	end

	TacticSetNames.UpdateAllLabels()
end

